from django.shortcuts import render
from django.http import HttpResponse
import urllib3
import json


# Create your views here.
def index(request):
    return render(request, 'index.html')

def getbooks(request):
    http = urllib3.PoolManager()
    response = json.dumps({"result" : "https://www.googleapis.com/books/v1/volumes?q=" + request.GET.get('ttl')})
    return HttpResponse(response)
