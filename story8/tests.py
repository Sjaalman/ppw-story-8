from django.test import TestCase, Client
from django.urls import reverse
import json
# Create your tests here.

class story7Test(TestCase):
    def test_GET_index(self):
        response = self.client.get(reverse('story8:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,"index.html")

    def test_GET_API(self):
        response = self.client.get(reverse('story8:getbooks'), {'ttl' : 'Das Kapital'})
        self.assertEqual(response.status_code, 200)
        self.assertIn('Das Kapital', response.content.decode('utf-8'))
        

