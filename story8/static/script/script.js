$(document).ready(function(){
    // Get JSON book
    $("#find-btn").click(() => {
        var title = $("#book-form").val()

        if (title === "") {
            alert("Tolong masukkan input yang valid")
        } else {
            $("#book-list").empty()
            try {
                $.ajax({
                    url: 'getbooks',
                    type: 'get',
                    data: {ttl: title},
                    success: function(response) {
                        response = $.parseJSON(response)
                        $.get(response.result, (result) => {
                            for (book of result.items) {
                                update_table(book)
                            }
                            $("#found-amount").text("Hasil Pencarian " + "(" + result.items.length + ")" + " :")
                        })
                    }
                })
            } catch {
                alert("Buku dengan judul tersebut tidak dapat ditemukan.")
            }
        }
    })

    function update_table(book) {
        var table = $("#book-list")
        try {
            var thumbnail = book.volumeInfo.imageLinks.thumbnail
        } catch {
            var thumbnail = "https://upload.wikimedia.org/wikipedia/commons/f/fc/No_picture_available.png"
        }
        var description = book.volumeInfo.description ? book.volumeInfo.description : "No description available"
 
        table.append(
            `<div class="card w-75 border-dark m-3">
                <h3 class="card-header bg-main1 text-center text-left-lg">${book.volumeInfo.title}</h3>
                <div class="card-body bg-main2">
                    <div class="row">
                        <div class="col-lg-3 col-12 d-flex align-items-center justify-content-center">
                            <img class="mr-3 book-cover" src="${thumbnail}">
                        </div>
                        <div class="col-lg-9 col-12 book-info">
                            <h5 class="card-title">Published on ${book.volumeInfo.publishedDate}</h5>
                            <h6>Deskripsi: </h6>
                            <p class="card-text text-justify">${description}</p>
                        </div>
                    </div>
                </div>
            </div>`
        )

        $(".result-section").css({"display" : "block"})
    }

    $(".navbar-brand").click(() => {
        $(".result-section").css({"display" : "none"})
    })

})
